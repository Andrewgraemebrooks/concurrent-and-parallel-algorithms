package lib;

import java.util.concurrent.Semaphore;

public class MathsThread1 extends Thread {
	// Fields
	private Counter counter;
	private Semaphore mutex1;
	private Semaphore mutex2;
	// Constructors
	public MathsThread1(Counter counter, Semaphore mutex1, Semaphore mutex2) {
		this.counter = counter;
		this.mutex1 = mutex1;
		this.mutex2 = mutex2;
	}
	// Methods
	public void run() {
		try {
			mutex1.acquire();
			int x = counter.getValue();
			System.out.println("Increasing given value " + x + " by 5...");
			counter.setValue(x + 5);
			System.out.println("Increasing complete!");
			mutex2.release();
		} catch (InterruptedException ie) {
			System.out.println(ie.getMessage());
		}
	}

}
