package lib;

import java.util.concurrent.*;

public class Producer extends Thread{
	private int[] ar;
	private Semaphore empty;
	private Semaphore full;
	CounterTwo in;
	private Semaphore inAvaliable;

	public Producer(int[] a, Semaphore e, Semaphore f, CounterTwo in, Semaphore inAvaliable) {
		ar = a;
		empty = e;
		full = f;
		this.in = in;
		this.inAvaliable = inAvaliable;
	}
	
	public void run() {
		try {
			for(int i = 0; i < 10; i++) {
				empty.acquire();
				inAvaliable.acquire();
				int inValue = in.getValue();
				ar[inValue] = i ;
				System.out.println("added " + i + " at index in = " + inValue);
				in.setValue((inValue + 1) % ar.length);
				System.out.println("Next available slot at " + "index in = " + inValue);
				//System.out.println("No. of Available slot: " + "empty = " + empty.availablePermits());
				System.out.println("\n");
				inAvaliable.release();
				full.release();
			}
		}
		catch(InterruptedException e) {
			System.out.println(e.getMessage());
		}
	
}
}
