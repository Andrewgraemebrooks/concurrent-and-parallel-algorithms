package lib;

public class Counter {
	// Fields
	private int value;
	// Constructors
	public Counter(int value) {
		this.value = value;
	}
	// Methods
	public int getValue() {
		return value;
	}
	
	public void setValue(int value) {
		this.value = value;
	}

}
