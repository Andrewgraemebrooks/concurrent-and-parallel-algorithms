package lib;

import java.util.concurrent.Semaphore;

public class MathsThread2 extends Thread {
	// Fields
	private Counter counter;
	private Semaphore mutex1;
	private Semaphore mutex2;
	// Constructors
	public MathsThread2(Counter counter, Semaphore mutex1, Semaphore mutex2) {
		this.counter = counter;
		this.mutex1 = mutex1;
		this.mutex2 = mutex2;
	}
	// Methods
	public void run() {
		try {
			mutex2.acquire();
			int x = counter.getValue();
			System.out.println("Decreasing given value " + x + " by 2...");
			counter.setValue(x - 2);
			System.out.println("Decreasing complete!");
			mutex1.release();
		} catch (InterruptedException ie) {
			System.out.println(ie.getMessage());
		}
	}

}
