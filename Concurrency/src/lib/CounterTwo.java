package lib;

public class CounterTwo {
	// Fields
	private int value;
	// Constructors
	public CounterTwo(int value) {
		this.value = value;
	}
	// Methods
	public int getValue() {
		return value;
	}
	
	public void setValue(int value) {
		this.value = value;
	}

}
