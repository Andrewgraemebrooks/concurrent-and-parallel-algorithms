package lib;

import java.util.concurrent.*;

public class Consumer extends Thread{
	private int[] ar;
	private Semaphore empty;
	private Semaphore full;
	CounterTwo out;
	int val;
	Semaphore outAvaliable;

	public Consumer(int[] a, Semaphore e, Semaphore f, CounterTwo out, Semaphore outAvaliable) {
		ar = a;
		empty = e;
		full = f;
		this.out = out;
		this.outAvaliable = outAvaliable;
	}
	
	public void run() {
		try {
			
			
			for(int i = 0; i < 10; i++) {
				full.acquire();
				outAvaliable.release();
				int outValue = out.getValue();
				val = ar[outValue];			 
				ar[outValue] = 0;
				System.out.println("removed " + val + " at position " + outValue);				
				out.setValue((outValue + 1) % ar.length);
				System.out.println("\n");
				outAvaliable.release();
				empty.release();
			}
							
		}
		catch(InterruptedException e) {
			System.out.println(e.getMessage());
		}
	}


}
