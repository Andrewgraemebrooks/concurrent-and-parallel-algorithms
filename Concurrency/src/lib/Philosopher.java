package lib;

import java.util.concurrent.Semaphore;

public class Philosopher extends Thread{
	// Fields
	Semaphore left;
	Semaphore right;
	int leftChop;
	int rightChop;
	int id;
	boolean reverse;
	
	// Constructors
	public Philosopher(Semaphore left, Semaphore right, int leftChop, int rightChop, int id, boolean reverse) {
		this.left = left;
		this.right = right;
		this.leftChop = leftChop;
		this.rightChop = rightChop;
		this.id = id;
		this.reverse = reverse;
	}
	
	// Methods
	public void run() {
		try {
			for(int i = 0; i < 5; i++) {
				System.out.println("Philosopher " + id + " is thinking...");
				Thread.sleep((int)(Math.random() * 100));
				if (reverse) {
					right.acquire();
					System.out.println("Philosopher " + id + " is picking up the right chopstick!");
					left.acquire();
					System.out.println("Philosopher " + id + " is picking up the left chopstick!");
				}
				else {
					left.acquire();
					System.out.println("Philosopher " + id + " picks up the left chopstick!");				
					right.acquire();
					System.out.println("Philosopher " + id + " picks up the right chopstick!");
				}
				System.out.println("Philosopher " + id + " starts eating");
				int random = (int)(Math.random() * 100);
				Thread.sleep(random);
				System.out.println("Philosopher " + id + " drops the left and right chopstick!");
				left.release();
				right.release();
				
			}
		} catch (InterruptedException e) {
			System.out.println(e.getMessage());
		}
	}
}
