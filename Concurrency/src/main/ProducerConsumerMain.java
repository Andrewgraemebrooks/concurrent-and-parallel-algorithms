package main;

import lib.*;
import java.util.concurrent.*;

public class ProducerConsumerMain {

	public static void main(String[] args) throws InterruptedException{
        int[] arr = new int[5];
		
		Semaphore empty = new Semaphore(4);
		Semaphore full = new Semaphore(0);
		Semaphore inAvaliable = new Semaphore(1);
		Semaphore outAvaliable = new Semaphore(1);
		
		CounterTwo in = new CounterTwo(0);
		CounterTwo out = new CounterTwo(0);
		
		
		Producer m1 = new Producer(arr, empty, full, in, inAvaliable);
	    Consumer m2 = new Consumer(arr, empty, full, out, outAvaliable);
	    Producer m3 = new Producer(arr, empty, full, in, inAvaliable);
	    Consumer m4 = new Consumer(arr, empty, full, out, outAvaliable);
		
		m1.start();
		m2.start();
		m3.start();
		m4.start();
		m1.join();
		m2.join();
		m3.join();
		m4.join();
		

	}

}
