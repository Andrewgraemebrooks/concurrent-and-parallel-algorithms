package main;

import java.util.concurrent.Semaphore;

import lib.Philosopher;

public class Dining {
	public static void main(String[] args) throws InterruptedException {
		Semaphore f0 = new Semaphore(1);
		Semaphore f1 = new Semaphore(1);
		Semaphore f2 = new Semaphore(1);
		Semaphore f3 = new Semaphore(1);
		Semaphore f4 = new Semaphore(1);
		
		Philosopher p0 = new Philosopher(f0,f1,0,1,0,false);
		Philosopher p1 = new Philosopher(f1,f2,1,2,1,false);
		Philosopher p2 = new Philosopher(f2,f3,2,3,2,false);
		Philosopher p3 = new Philosopher(f3,f4,3,4,3,false);
		Philosopher p4 = new Philosopher(f4,f0,4,0,4,true);
		
		p0.start();
		p1.start();
		p2.start();
		p3.start();
		p4.start();

	}
}
