package main;

import lib.Counter;
import lib.MathsThread1;
import lib.MathsThread2;
import java.util.concurrent.*;

public class MathsMain {
	public static void main(String[] args) throws InterruptedException {
		Counter counter = new Counter(5);
		Semaphore mutex1 = new Semaphore(1);
		Semaphore mutex2 = new Semaphore(0);
		MathsThread1 m1 = new MathsThread1(counter, mutex1, mutex2);
		MathsThread2 m2 = new MathsThread2(counter, mutex1, mutex2);
		MathsThread1 m3 = new MathsThread1(counter, mutex1, mutex2);
		MathsThread2 m4 = new MathsThread2(counter, mutex1, mutex2);
		
		m1.start();
		m2.start();
		m3.start();
		m4.start();
		m1.join();
		m2.join();
		m3.join();
		m4.join();
		System.out.println(counter.getValue());
		
	}
}
